## Introduction

This lab session is a low-level, hands-on introduction to container security using the Red Hat Enterprise Linux 8 command line interface. It is intended to be consumed as a series of self paced exercises.

### Prerequisites

* An introductory knowledge of Linux and containers is helpful.
* Basic text editing skills using vim or nano.

#### Lab Environment

![Lab Diagram]({% image_path lab-diagram.png %})

#### System Information

You will be working with the following systems running Red Hat Enterprise Linux 8 beta. 

* {{BASTION}} (Container Engine and ssh access)
* {{SERVER_0}} (Container Registry)

##### Lab Guide
This lab guide is hosted at [https://bit.ly/2GVASG8](https://bit.ly/2GVASG8). You can access the [source](https://gitlab.com/redhatsummitlabs/a-practical-introduction-to-container-security-using-cri-o) as well.

##### If you find bugs
Please post them in the [etherpad](http://f29summitlab-summittfdd21bkozde-fcugphac.srv.ravcloud.com:9001/p/security).

**Recommendation:**  Make a **backup** copy before modifying any file on the systems.  

##### Conventions used in this lab 

Any example that begins with the ```$``` prompt is meant to be executed on the container host.

For example:

~~~shell
$ ls /usr/tmp
~~~

or

~~~shell
$ sudo ls /var/lib/containers
~~~

Any example that begins with the ```{{CONTAINER_PROMPT}}``` prompt is meant to be executed in the container's namespace.

For example:

~~~shell
{{CONTAINER_PROMPT}} ls /
~~~

Commands are shown using fixed width font followed by a
blank line along with any output.

For example:

~~~shell
$ podman ps -a

CONTAINER ID  IMAGE                       COMMAND    CREATED      STATUS   PORTS  NAMES
5d6230fd6fdd  localhost/rhel-ubi8:latest  sleep 999  9 hours ago  Created         sleepy
~~~

#### Getting Started Exercise

1) If you have not already obtained a Global User ID (GUID), visit [GUID grabber](https://www.opentlc.com/gg/gg.cgi?profile=generic_tester). The lab key is **podman**.

2) On your laptop, open a terminal window. Refer to the **activities** menu in the upper left of the screen.

3) Make a backup copy of ```$HOME/.bashrc```

~~~shell
$ cp .bashrc .bashrc.bak
~~~

4) Set your **GUID** environment variable to that value you obtained above so
that any additional windows or tabs that open inherit this variable.

~~~shell
$ echo "export GUID=insert-guid-here" >> .bashrc
$ . .bashrc
~~~

5) Open a **new** terminal window or tab and confirm that the GUID variable is properly exported and set.

~~~shell
$ echo $GUID

ead3
~~~

6) Using an ssh client on your laptop, login to the client system as ```lab-user```. The
laptops are pre-loaded with the proper ssh keys so it should not prompt for 
a password. Password authentication is also enabled as ```lab-user``` and password ```r3dh4t1!```. 


~~~shell
$ ssh lab-user@{{BASTION}}
~~~

7) From ```{{BASTION}}```, confirm you can use ```ssh``` to login to ```{{SERVER_0}}```.

~~~shell
$ ssh {{SERVER_0}}
~~~

8) Confirm that you can access privileged resources on both systems without a password.

~~~shell
$ sudo ls /var/lib/containers/
~~~
