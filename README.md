# A Practical Introduction to Container Security

This lab manual is written in intended to run using the [workshopper](https://github.com/openshift-evangelists/workshopper) software. Use a Linux container engine like ```podman``` or ```docker``` to host it.

For example.

```
$ sudo podman run -p 9002:8080 -it --rm --name=lab-guide -e CONTENT_URL_PREFIX="https://gitlab.com/redhatsummitlabs/a-practical-introduction-to-container-security-using-cri-o/raw/master/" -e WORKSHOPS_URLS="https://gitlab.com/redhatsummitlabs/a-practical-introduction-to-container-security-using-cri-o/raw/master/_workshop1.yml" quay.io/osevg/workshopper
```

Then visit [http://localhost:9002](http://localhost:9002) to access the content.