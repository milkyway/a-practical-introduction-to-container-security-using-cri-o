## Registry Configuration
During this lab you will configure ```{{SERVER_0}}``` to host a Quay container registry. The registry 
consists of (3) containers (**quay**, **mysql** and **redis**). These containers have been loaded 
but are not running. Also, the
Quay container must be configured before it runs. 

#### Exercise: Registry Configuration

##### Overview 

* Configure the quay registry with the mysql and redis IPs.
* Use curl to test connectivity to the registry services.

##### How to

**=>:** Perform this lab exercise on the ```{{SERVER_0}}``` server.

Start the ```mysql``` and ```redis``` containers.

~~~shell
$ sudo podman start mysql redis
~~~

Next, obtain the IP addresses of the ```mysql``` and ```redis```
containers and use them to configure the ```quay``` container.

~~~shell
$ sudo podman inspect mysql | grep IPAddress
$ sudo podman inspect redis | grep IPAddress
~~~

**=>:** Make a backup copy of ```/pv/quay/config/config.yaml```.

~~~shell
$ sudo cp ```/pv/quay/config/config.yaml``` ```/pv/quay/config/config.yaml.bak```
~~~

Use a text editor (```vim``` or ```nano```) to modify ```/pv/quay/config/config.yaml``` at
the following lines:

Modify line 5 with the correct IP of the ```mysql``` container.

```DB_URI: mysql+pymysql://root:L36PrivxRB02bqOB9jtZtWiCcMsApOGn@10.88.0.X/enterpriseregistrydb```

Modify Lines 4 and 49 with the correct IP of the ```redis``` container.

```BUILDLOGS_REDIS: {host: 10.88.0.X, port: 6379}```

```USER_EVENTS_REDIS: {host: 10.88.0.X, port: 6379}```

Now start the ```quay``` container and test it. It may take a minute 
or so for the registry to become ready.

~~~shell
$ sudo podman start quay
~~~

Checking the Registry

~~~shell
$ curl http://localhost:/v2/_catalog
~~~

Expected output:

~~~shell
{"repositories":[]}
~~~

To have a look at the Quay Web UI, visit your quay registry at ```http://server-$GUID.rhpds.opentlc.com``` 
from a web browser and login as ```quayuser/L36PrivxRB02bqOB9jtZtWiCcMsApOGn```

**=>** The quay registry configuration is now complete. All of the remaining lab exercises will
be performed on the {{BASTION}} system.

**=>** Configure the container engine to use the registry. Edit ```/etc/containers/registries.conf``` and confirm that
{{SERVER_0}} is configured as an insecure registry (it's not using SSL). 

From ```{{BASTION}}```, use ```curl``` to check basic networking to the registry.

~~~shell
$ curl http://{{SERVER_0}}:/v2/_catalog
~~~

From ```{{BASTION}}```, use ```podman``` to login to the Quay registry.

~~~shell
$ podman login --username=quayuser --password=L36PrivxRB02bqOB9jtZtWiCcMsApOGn {{SERVER_0}}
~~~

Expected output:

~~~shell
Login Succeeded!
~~~

**NOTE:** The Quay registry and client system ```({{BASTION}})``` are configured to not use SSL. If you are interested how this is done on the client side, have a look at ```/etc/containers/registries.conf``` 
near the ```[registries.insecure]``` block.

#### Exercise: Tagging and pushing images to a remote registry

##### Overview
* Loading a container image from an archive. 
* Tag and push an image to a remote registry.

##### Howto

Load the RHEL image from the images directory.

~~~shell
$ gzip --decompress --stdout images/rhel-ubi8.tar.gz |podman load
~~~

Verify the image loaded.

~~~shell
$ podman images
~~~

Expected Output:

~~~shell
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
{{RHEL_CONTAINER}}                   latest              c82b952c1204        10 months ago       123.4 MB
~~~

Now tag and push the image to the remote registry hosted at {{SERVER_0}}.

~~~shell
$ podman tag {{RHEL_CONTAINER}} {{SERVER_0}}/quayuser/rhel-ubi8:latest
~~~

Confirm the tag is correct.

~~~shell
$ podman images
~~~

Expected Output:

~~~shell
REPOSITORY                             TAG      IMAGE ID       CREATED        SIZE
localhost/rhel-ubi8                    latest   cc7efd763847   2 months ago   216 MB
server.summit.lab/quayuser/rhel-ubi8   latest   cc7efd763847   2 months ago   216 MB
~~~

Finally, push the image to the {{SERVER_0}} registry.

~~~shell
$ podman push {{SERVER_0}}/quayuser/rhel-ubi8:latest
~~~

Expected Output:

~~~shell
Getting image source signatures
Copying blob 8da573feae5f: 205.77 MiB / 205.77 MiB [========================] 5s
Copying blob 6ef321d2357f: 10.00 KiB / 10.00 KiB [==========================] 5s
Copying config cc7efd763847: 0 B / 4.36 KiB [-------------------------------] 0s
Writing manifest to image destination
Writing manifest to image destination
Storing signatures
~~~

#### Exercise: Pulling images from a remote registry

##### Overview

* Saving and removing images from the container run-time.
* Pulling an image from a remote registry.

##### Howto

If the push was successful, delete the local cached copy and 
pull a new image from the remote registry on {{SERVER_0}}. 

~~~shell
$ podman rmi {{SERVER_0}}/quayuser/rhel-ubi8:latest
$ podman pull {{SERVER_0}}/quayuser/rhel-ubi8:latest
~~~

Expected Output:

~~~shell
Using default tag: latest
Trying to pull repository {{SERVER_0}}/quayuser/rhel-ubi8:latest ... 
sha256:6b079ae764a6affcb632231349d4a5e1b084bece8c46883c099863ee2aeb5cf8: Pulling from ...
Digest: sha256:6b079ae764a6affcb632231349d4a5e1b084bece8c46883c099863ee2aeb5cf8
Status: Downloaded newer image for {{SERVER_0}}/quayuser/mystery:latest
~~~